#/bin/bash

. /usr/share/neteye/scripts/rpm-functions.sh

PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:
export PATH

function get_node_running_service(){
    if is_cluster ; then
        SERVICE_NAME=$1
        NODE_NAME=""
        NODE_NAME=$( pcs status | grep ${SERVICE_NAME}_drbd_fs.*Started | grep -o "Started .*$" | sed s/"Started "//g )
        RES=$?

        if [[ $RES -ne 0 ]]; then
            echo "Something wrong with pcs status: got ${RES}"
            exit 1
        fi

        if [[ "${NODE_NAME}" == "" ]]; then
            echo "Unable to retrieve service ${SERVICE_NAME} please check syntax!"
            exit 1
        fi

        echo "${NODE_NAME}"
    else
        echo "localhost"
    fi
}

function test_or_configure_ssh(){
    if ! is_cluster ; then
        ssh -o BatchMode=yes root@localhost exit
        RES=$?

        if [[ $RES -ne 0 ]]; then
            echo "Configuring SSH keys to login using localhost"
            ssh -t root@localhost "ssh-keygen -t rsa -N '' -f /root/.ssh/id_rsa";
            ssh -t root@localhost ssh-copy-id root@localhost
        fi
    fi
}

function get_nodes(){
    if is_cluster ; then
        echo $( get_cluster_and_elastic_only_nodes_hostname)
    else
        echo "localhost"
    fi
}

function help(){
    echo -e "This utility provides backup features for NetEye 4 instaces both single node and cluster."
    echo -e "Please specify one or more of the following options. Note that -d is mandatory!"
    echo -e "\t -a \t backup Apache, Nginx configuration"
    echo -e "\t -c \t save cluster configurations"
    echo -e "\t -d <dir> directory where backups will be saved permanently"
    echo -e "\t -e \t backup elasticsearch configuration an license"
    echo -e "\t -f <file> custom list of files to backup"
    echo -e "\t -g \t save telegraf (grafana) configuration"
    echo -e "\t -i \t dump InfluxDB database"
    echo -e "\t -I \t backup InfluxDB database using LVM snapshot (suggested for huge dataset)"
    echo -e "\t -l \t backup logstash and filebeat configuration"
    echo -e "\t -m \t dump MariaDB database using LVM utility"
    echo -e "\t -n \t backup icinga and icinga master configuration"
    echo -e "\t -o \t backup OCS Invenotry Server e Reports configuration"
    echo -e "\t -p \t save PCS status output (not available for single node)"
    echo -e "\t -r \t save a list of installed RPMs"
    echo -e "\t -s \t save nats configuration"
    echo -e "\t -t <dir> specify temporary backup directory. This directory will"
    echo -e "\t \t be used to store backups temporarily such as influxdb and MariaDB dumps"
    echo -e "\t \t before to copy them to backup directory. By default /tmp/neteye_backup/"
    echo -e "\t \t will be used, be use to have enough space in /tmp to store dumps."
    echo -e "\t -v \t save nagvis configuration"
    echo -e "\t -w \t save tornado configuration"
    echo -e "\t -z \t archive backup as .tar.bz2"
    echo -e "\t -h \t print this message"

    exit 0
}

#########################################################################
# Execute and check input options
#########################################################################

TIMESTAMP=$( date +%Y%m%d_%H%M%S )

while getopts ":acd:ef:ghiIlmnoprst:vwz" opt; do
  case ${opt} in
    a ) APACHE=1
      ;;
    c ) CLUSTER_CONFIG=1
      ;;
    d ) BACKUP_DIR="$OPTARG"
        mkdir -p $BACKUP_DIR
      ;;
    e ) ELASTICSEARCH=1
      ;;
    f )
        FILEOPT=1
        FILE_LIST="$OPTARG"
        if [[ ! -f $FILE_LIST ]]; then
            echo "[!] File list $FILE_LIST is not regular file!"
            exit 1
        fi
      ;;
    g ) GRAFANA=1
      ;;
    h ) help
      ;;
    i ) INFLUX=1
      ;;
    I ) INFLUX_LVM=1
      ;;
    l ) LOGSTASH=1
        FILEBEAT=1
      ;;
    m ) MYSQL=1
      ;;
    n ) ICINGA=1
      ;;
    o ) OCS=1
      ;;
    p ) PCS=1
      ;;
    r ) RPM=1
      ;;
    s ) NATS=1
      ;;
    t ) TMP_BACKUP_DIR="$OPTARG/$TIMESTAMP"
      ;;
    v ) NAGVIS=1
      ;;
    w ) TORNADO=1
      ;;
    z ) ZIP=1
      ;;
    \? ) echo "Option: -$OPTARG is not supported! Use -h option for help"
       exit 1
    ;;
  esac
done

if [[ -z $BACKUP_DIR ]]; then
    echo "[!] Backup dir is mandatory!"
    exit 1
fi

if [[ -z $TMP_BACKUP_DIR ]]; then
    TMP_BACKUP_DIR="/tmp/neteye_backup/$TIMESTAMP"
    echo "[!] WARNING: you are using default tmp dir which is located in $TMP_BACKUP_DIR. Please be sure to have enough free space"
fi

# Configure backup error file
export TMP_BACKUP_ERROR_FILE="$TMP_BACKUP_DIR/errors.txt"


# On single node configure ssh to be used for local login
test_or_configure_ssh

#########################################################################
# Backups to be executed on each node of the cluster
#########################################################################

# Add in this section backups for scripts to be run on each node e.g. those installed in /neteye/local/

NODES=$( get_nodes )

for TARGET_NODE in ${NODES}
do
    if [[ -n ${CLUSTER_CONFIG} ]]; then
        echo "[+] Executing cluster configuration backup"
        sh /neteye/shared/backup/scripts/cluster_config_backup.sh $TARGET_NODE "${TMP_BACKUP_DIR}/cluster_config/"
    fi

    if [[ -n ${FILEOPT} ]]; then
        echo "[+] Executing file list backup"
        sh /neteye/shared/backup/scripts/file_backup.sh $TARGET_NODE "${TMP_BACKUP_DIR}/files/" "$FILE_LIST"
    fi

    if [[ -n ${ELASTICSEARCH} ]]; then
        echo "[+] Executing elasticsearch backup"
        sh /neteye/shared/backup/scripts/elasticsearch_backup.sh $TARGET_NODE "${TMP_BACKUP_DIR}/elasticsearch/"
    fi

    if [[ -n ${RPM} ]]; then
        echo "[+] Executing rpm list backup"
        sh /neteye/shared/backup/scripts/rpm_list_backup.sh $TARGET_NODE "${TMP_BACKUP_DIR}/rpm/"
    fi
    if [[ -n ${ICINGA} ]]; then
        echo "[+] Executing Neteye Local backup"
        sh /neteye/shared/backup/scripts/neteye_local_backup.sh $TARGET_NODE "${TMP_BACKUP_DIR}/neteye-local/"
    fi

done



#########################################################################
# Backups to be executed only once for the whole cluster
#########################################################################

if [[ -n ${PCS} ]]; then
    echo "[+] Executing pcs status backup"
    sh /neteye/shared/backup/scripts/pcs_backup.sh "${TMP_BACKUP_DIR}/pcs/"
fi



#########################################################################
# Backups to be executed on a specific node where the service is running
#########################################################################

# Add in this section PCS/DRBD services which runs only on a specific node
# You can copy one of the "if sections" below and costumize, pay attention to the name of the service because
# sometimes it does not corresponde to the service name.
# Check your PCS status and copy the name ending with "_drbd_fs"

if [[ -n ${APACHE} ]]; then
    echo "[+] Executing apache backup"
    TARGET_NODE=$( get_node_running_service "httpd" )
    echo "[i] TARGET_NODE=$TARGET_NODE"
    sh /neteye/shared/backup/scripts/httpd_backup.sh ${TARGET_NODE} "${TMP_BACKUP_DIR}/httpd/"
    
    echo "[+] Executing nginx backup"
    TARGET_NODE=$( get_node_running_service "nginx" )
    echo "[i] TARGET_NODE=$TARGET_NODE"
    sh /neteye/shared/backup/scripts/nginx_backup.sh ${TARGET_NODE} "${TMP_BACKUP_DIR}/nginx/"
fi

if [[ -n ${GRAFANA} ]]; then
    echo "[+] Executing grafana backup"
    TARGET_NODE=$( get_node_running_service "grafana" )
    echo "[i] TARGET_NODE=$TARGET_NODE"
    sh /neteye/shared/backup/scripts/telegraf_backup.sh ${TARGET_NODE} "${TMP_BACKUP_DIR}/telegraf/"
fi

if [[ -n ${INFLUX} ]]; then
    echo "[+] Executing influxdb backup"
    TARGET_NODE=$( get_node_running_service "influxdb" )
    echo "[i] TARGET_NODE=$TARGET_NODE"
    sh /neteye/shared/backup/scripts/influxdb_backup.sh ${TARGET_NODE} "${TMP_BACKUP_DIR}/influxdb/" "${TMP_BACKUP_DIR}/influxdb"
fi

if [[ -n ${INFLUX_LVM} ]]; then
    echo "[+] Executing influxdb LVM backup"
    TARGET_NODE=$( get_node_running_service "influxdb" )
    echo "[i] TARGET_NODE=$TARGET_NODE"
    if [[ ${TARGET_NODE} == "localhost" ]]; then
        sh /neteye/shared/backup/scripts/influxdb_lvm_single_backup.sh ${TARGET_NODE} "${TMP_BACKUP_DIR}/influxdb/" "${TMP_BACKUP_DIR}/influxdb/"
    else
        sh /neteye/shared/backup/scripts/influxdb_lvm_backup.sh ${TARGET_NODE} "${TMP_BACKUP_DIR}/influxdb/" "${TMP_BACKUP_DIR}/influxdb/"
    fi
fi

if [[ -n ${ICINGA} ]]; then
    echo "[+] Executing icinga2-master backup"
    TARGET_NODE=$( get_node_running_service "icinga2-master" )
    echo "[i] TARGET_NODE=$TARGET_NODE"
    sh /neteye/shared/backup/scripts/icinga2master_backup.sh ${TARGET_NODE} "${TMP_BACKUP_DIR}/icinga2-master/"
fi

if [[ -n ${ICINGA} ]]; then
    echo "[+] Executing icingaweb2 backup"
    TARGET_NODE=$( get_node_running_service "icingaweb2" )
    echo "[i] TARGET_NODE=$TARGET_NODE"
    sh /neteye/shared/backup/scripts/icingaweb2_backup.sh ${TARGET_NODE} "${TMP_BACKUP_DIR}/icingaweb2/"
fi

if [[ -n ${ICINGA} ]]; then
    echo "[+] Executing monitoring backup"
    TARGET_NODE=$( get_node_running_service "icinga2-master" )
    echo "[i] TARGET_NODE=$TARGET_NODE"
    sh /neteye/shared/backup/scripts/monitoring_backup.sh ${TARGET_NODE} "${TMP_BACKUP_DIR}/monitoring/"
fi

if [[ -n ${LOGSTASH} ]]; then
    echo "[+] Executing logstash backup"
    TARGET_NODE=$( get_node_running_service "logstash" )
    echo "[i] TARGET_NODE=$TARGET_NODE"
    sh /neteye/shared/backup/scripts/logstash_backup.sh ${TARGET_NODE} "${TMP_BACKUP_DIR}/logstash/"
fi

if [[ -n ${LOGSTASH} ]]; then
    echo "[+] Executing elastic-blockchain-proxy backup"
    TARGET_NODE=$( get_node_running_service "elastic-blockchain-proxy" )
    echo "[i] TARGET_NODE=$TARGET_NODE"
    sh /neteye/shared/backup/scripts/elastic-blockchain-proxy_backup.sh ${TARGET_NODE} "${TMP_BACKUP_DIR}/elastic-blockchain-proxy/"
fi

if [[ -n ${FILEBEAT} ]]; then
    echo "[+] Executing filebeat backup"
    TARGET_NODE=$( get_node_running_service "filebeat" )
    echo "[i] TARGET_NODE=$TARGET_NODE"
    sh /neteye/shared/backup/scripts/logstash_backup.sh ${TARGET_NODE} "${TMP_BACKUP_DIR}/filebeat/"
fi

if [[ -n ${NAGVIS} ]]; then
    echo "[+] Executing nagvis backup"
    TARGET_NODE=$( get_node_running_service "nagvis" )
    echo "[i] TARGET_NODE=$TARGET_NODE"
    sh /neteye/shared/backup/scripts/nagvis_backup.sh ${TARGET_NODE} "${TMP_BACKUP_DIR}/nagvis/"
fi

if [[ -n ${NATS} ]]; then
    echo "[+] Executing nats backup"
    TARGET_NODE=$( get_node_running_service "nats-server" )
    echo "[i] TARGET_NODE=$TARGET_NODE"
    sh /neteye/shared/backup/scripts/nats_backup.sh ${TARGET_NODE} "${TMP_BACKUP_DIR}/nats/"
fi

if [[ -n ${OCS} ]]; then
    echo "[+] Executing OCS Inventory backup"
    TARGET_NODE=$( get_node_running_service "ocsinventory-server" )
    echo "[i] TARGET_NODE=$TARGET_NODE"
    sh /neteye/shared/backup/scripts/ocs-inventory_backup.sh ${TARGET_NODE} "${TMP_BACKUP_DIR}/ocs-inventory/"
fi

if [[ -n ${OCS} ]]; then
    echo "[+] Executing GLPI backup"
    TARGET_NODE=$( get_node_running_service "glpi" )
    echo "[i] TARGET_NODE=$TARGET_NODE"
    sh /neteye/shared/backup/scripts/glpi_backup.sh ${TARGET_NODE} "${TMP_BACKUP_DIR}/glpi/"
fi

if [[ -n ${TORNADO} ]]; then
    echo "[+] Executing tornado backup"
    TARGET_NODE=$( get_node_running_service "tornado" )
    echo "[i] TARGET_NODE=$TARGET_NODE"
    sh /neteye/shared/backup/scripts/tornado_backup.sh ${TARGET_NODE} "${TMP_BACKUP_DIR}/tornado/"
fi

if [[ -n ${TORNADO} ]]; then
    echo "[+] Executing tornado_email_collector backup"
    TARGET_NODE=$( get_node_running_service "tornado_email_collector" )
    echo "[i] TARGET_NODE=$TARGET_NODE"
    sh /neteye/shared/backup/scripts/tornado_email_backup.sh ${TARGET_NODE} "${TMP_BACKUP_DIR}/tornado_email_collector/"
fi

if [[ -n ${TORNADO} ]]; then
    echo "[+] Executing tornado_icinga2_collector backup"
    TARGET_NODE=$( get_node_running_service "tornado_icinga2_collector" )
    echo "[i] TARGET_NODE=$TARGET_NODE"
    sh /neteye/shared/backup/scripts/tornado_icinga2_backup.sh ${TARGET_NODE} "${TMP_BACKUP_DIR}/tornado_icinga2_collector/"
fi

if [[ -n ${TORNADO} ]]; then
    echo "[+] Executing tornado_webhook_collector backup"
    TARGET_NODE=$( get_node_running_service "tornado_webhook_collector" )
    echo "[i] TARGET_NODE=$TARGET_NODE"
    sh /neteye/shared/backup/scripts/tornado_webhook_backup.sh ${TARGET_NODE} "${TMP_BACKUP_DIR}/tornado_webhook_collector/"
fi
if [[ -n ${MYSQL} ]]; then
    echo "[+] Executing mariadb backup"
    TARGET_NODE=$( get_node_running_service "mariadb" )
    echo "[i] TARGET_NODE=$TARGET_NODE"
    if [[ ${TARGET_NODE} == "localhost" ]]; then
        sh /neteye/shared/backup/scripts/mariadb_single_backup.sh ${TARGET_NODE} "${TMP_BACKUP_DIR}/mariadb_tmp/" "${TMP_BACKUP_DIR}/mariadb/"
    else
        sh /neteye/shared/backup/scripts/mariadb_backup.sh ${TARGET_NODE} "${TMP_BACKUP_DIR}/mariadb_tmp/" "${TMP_BACKUP_DIR}/mariadb/"
    fi
fi

#########################################################################
# Compress backup folder
#########################################################################


if [[ -f "$TMP_BACKUP_ERROR_FILE" ]]; then
    echo "[!] Something went wrong during backup, you can found a partial backup in $TMP_BACKUP_ERROR_FILE and a list of errors in $TMP_BACKUP_ERROR_FILE"
    exit 1
else
    if [[ -n ${ZIP} ]]; then
        echo "[+] Compressing backup"
        tar -cf "${BACKUP_DIR}/$TIMESTAMP.tar.bz2" "${TMP_BACKUP_DIR}/" --use-compress-program=lbzip2
        echo "[+] Removing backup directory"
        rm -rf "${TMP_BACKUP_DIR}"
    else
        echo "[+] Moving backup direcotry without compressing it"
        cp -r "${TMP_BACKUP_DIR}" "${BACKUP_DIR}"
        echo "[+] Removing backup directory"
        rm -rf "${TMP_BACKUP_DIR}"
    fi

    echo "[+] Backup completed!"
fi