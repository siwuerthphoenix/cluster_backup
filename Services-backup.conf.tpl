{
    "volume_group": "vg00",
    "ip_pre" : "192.168.1",
    "Services": [
        {
            "name": "backup",
            "ip_post": null,
            "drbd_minor": 210,
            "drbd_port": 7997,
            "folder": "/neteye/shared/backup/",
            "service":null,
            "collocation_resource": "cluster_ip",
            "size": "1024"
        }
    ]
}
