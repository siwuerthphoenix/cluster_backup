#! /bin/bash

# This script keeps only the most recent backups and delete others.
# Then number of backups to keep is specified by the user

while getopts "h" OPTION; do
    case $OPTION in
    h)
        echo "This script keeps only the specified number of archives in the specified folder."
        echo "For example:"
        echo "backup_rotate.sh /data/neteye_backup 3"
        echo "keeps only the 3 most recent .tar.bz2 in the folder /data/neteye_backup and delete others."
        echo "Usage:"
        echo "$0 <BACKUP_DIR> <COPIES_TO_KEEP>"
        exit 0
        ;;
    esac
done

BACKUP_DIR=$1
KEEP=$2
COUNTER=1

for FILE in $(ls -t $BACKUP_DIR/*.tar.bz2); do
        echo $COUNTER
        if [[ $COUNTER -gt  $KEEP ]]; then
                echo "delete $FILE"
                rm -f $FILE
        else
                echo "keep $FILE"
        fi
        COUNTER=$((COUNTER+1))
done