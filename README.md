# README #
This project aims to backup a NetEye4 clusterized installation saving folowing data:

- Mariadb dump
- Influxdb dump
- List of installed rpms
- Pcs status
- Elasticsearch configuration
- System configuration (network, cluster, hostnames...)
- Custom file list
- Iginca2 and icinga2-master configurations
- Logstash configuration

**Feel free to improve existing scripts or add new scripts!**

## Project structure and RPMS
This project can be built as RPM package:
```bash
# Create an achive: 
tar -czvf cluster_backup.tar.gz -C project_dir/ .

# Prepare to build
cp cluster_backup.tar.gz ~/rpmbuild/SOURCES/
cd ~/rpmbuild/SOURCES/
ln -s project_dir/cluster_backup.spec

# Build package
rpmbuild --target noarch -bb cluster_backup.spec 

# Package will be located in ~/rpmbuild/RPMS/noarch/
```

In the directory **RPMS** you will find both an RPM of the project and all the dependencies. **epel.repo** is also available in this directory in the case you miss some dependecy, but should not be required.

It is also possible to clone repository and use it without installing the package: you have to put **backup.sh** and **script/** in the directory **/neteye/shared/backup/** otherwise the script will not work.

# Architecture
This utility is composed by several script which backup a single service or set of files related to a service. Each script is self contained: it performs the backup, store result in the backup directory and performs cleanup if needed.

Another main component is the orchestrator *backup.sh* which performs several actions:

* Select the scripts to be executed accordingly with options specified
* Setup ssh login for single node installation
* Execute backup scripts in the appropriate manner: for clustered service backup will be executed only on the node running the service. For global configurations backup will be executed only once for the entire cluster. For node-specific configurations (e.g. elasticsearch) backups will be executed on each node. 

## Set up
### Neteye Single Node

In order run the backup script you should follow this procedure:

* Create the installation directory **/neteye/shared/backup/** 
* Create a backup directory

The procedure can be executed automatically executing the scritp **install_singlenode.sh**:

* Copy backup.sh backup_rotate.sh and scripts directory into **/neteye/shared/backup/** 
* Create one or more cronjobs on each node executing **sh /neteye/shared/backup/backup.sh**.

The first time backup.sh is executed on a single node installation it will configure ssh key and root password will be required. **Execute a first backup manually in order to provide password!**

### NetEye Cluster
The script can be executed putting it in a DRBD resource and creating a cronjob on each node of the cluster. PCS resource includes only filesystem and does not includes neither service nor ip. 
To create the resource create a directory **/neteye/shared/backup/** on each node and then execute cluster_service_setup.pl adapting the template provided:
```bash
/usr/share/neteye/scripts/cluster/cluster_service_setup.pl -c Services-backup.conf.tpl
```
In order run the script on each node you should follow this procedure:

* Create the installation directory **on each node** (**/neteye/shared/backup/**)
* Run cluster_service_setup.pl as explained above
* Create a backup directory **on each node** (example /data/backup)

**only on the node where the DRBD resource is active**:
The procedure can be executed automatically executing the scritp **install_singlenode.sh**

* Install mylvmbackup **on each node**
(http://www.lenzg.net/mylvmbackup/) --> perl dependency (https://centos.pkgs.org/7/epel-aarch64/perl-Config-IniFiles-2.79-1.el7.noarch.rpm.html)
* Install cluster_backup script 

* Create one or more cronjobs on each node executing **sh /neteye/shared/backup/backup.sh**. The script will be execute only on the node where the DRBD resource is active avoiding multiple backups.

The first time backup.sh is executed on a single node installation it will configure ssh key and root password will be required. **Execute a first backup manually in order to provide password!**


## Usage
Execute the script specifying options **-d** and at least one of the backup options (otherwise you will get an empty backup).
If you use default temporary dir (tmp/neteye_backup/) please ensure to have enough space in /tmp to allow a complete backup of InfluxDB and MariaDB.

Example for a cluster full backup:
```bash
/neteye/shared/backup/backup.sh -a -c -d /data/backup -e -f /root/list.txt -g -i -l -m -n -o -p -r -s -t /data/tmp -v -w -z

## NetEye Backup Cluster
0 1 * * * if [ -f "/neteye/shared/backup/backup.sh" ]; then /neteye/shared/backup/backup.sh -a -c -d /cifs/backup/neteye-cluster-backup -e -g -I -l -m -n -o -p -r -s -t /data/tmp -v -w -z > /root/backup_cluster_output.txt 2>&1; fi 
```

**Warnings**

* Options **-c** and **-p** should not be used on single-node instances because they are related to cluster features.
* Option **-f list.txt** is intended to personalize your backup: list.txt contains a list of files or directories to be saved in addition to default backups
* In the case MariaDB and in particular InfluxDB backups take a long time or a huge amount of space, and therefore are prone to failure, you should consider to create separate cronjobs for them 
* InfluxDB backup can be executed both using dump (-i option) or LVM (-I option). See dedicated section below to choose the best practice for your environment.
* lbzip2 download page --> https://download-ib01.fedoraproject.org/pub/epel/7/x86_64/Packages/l/lbzip2-2.5-1.el7.x86_64.rpm
### Backup Rotation
**backup_rotate.sh** is a script intended to keep only last n backups where n is given by the user. 

For example to **keep only last 3 backups saved in /data/neteye-backups** you should execute a command like the following:
```bash
/neteye/shared/backup/backup_rotate.sh /data/neteye-backups 3

## Backup Rotate
0 4 * * * if [ -f "/neteye/shared/backup/backup_rotate.sh" ]; then /neteye/shared/backup/backup_rotate.sh /cifs/backup/neteye-cluster-backup 3 > /root/backup_rotate_output.txt 2>&1; fi
```

You have to add is as a cronjob as done for backup.sh

as an alternative you can delete also backups older than n days but this is unsafe: in case of backup failure also old backups can be deleted.
For example to delete backupd **located in neteye-backups older than 10 days** you can add a cronjob like the following:
```bash
0 4 * * * find /data/neteye-backup -mtime 10 -type f -delete
```

### Backup Monitoring
To monitor the correct execution of backups you can use check_neteye_backup.
The following example throws a warning if the backup is older than 24 hours and a critical if the backup is older than 48 hours.
Times are expressed in minutes.
```bash
check_neteye_backup neteye-backups -w 1440 -c 2880
```
Command and Service Template are available in the basket included in this project.


# Backup scripts 

## MariaDB

Backup mariadb using LVM, it is possible to do this using [mylvmbackup](http://www.lenzg.net/mylvmbackup/) as suggested on [MariaDB backup and restore official page](https://mariadb.com/kb/en/backup-and-restore-overview/#lvm).
Mylvmbackup locks writes on mariaDB, performs a flush to ensure that all data are written on filesystem, then dumps the Logical Volume and archive it.
In the case you run the backup on a single node NetEye4 the script assumes that MariaDB is located in **lvneteye**.

A backup of **/root/.my.cnf** is also performed and will be useful when restoring database.

It is possibile to restore data copying directories conf, data and log to mysql directory and fixing ownership for dat and log. It is suggested to use a NetEye container in order to have correct paths. Then it is possible to explore database or create dump of one or more directories.
Files *aria_log.00000001* and *aria_log_control* may cause errors during mariadb startup due to a bug and can be deleted.

### Dependeces
Dependeces are included in RPMS directory in this project:
* mylvmbackup [download RPM](http://www.lenzg.net/mylvmbackup/mylvmbackup-0.16-0.noarch.rpm)
* perl-Config-IniFiles perl-IO-stringy (epel repo)
* perl-File-Copy-Recursive perl-Sys-Syslog (base repo)

On RHEL8 the lbzip2 package is missing, install it with
```yum install https://download-ib01.fedoraproject.org/pub/epel/7/x86_64/Packages/l/lbzip2-2.5-1.el7.x86_64.rpm```


## InfluxDB

Backup can be done natively accoringly with [official documentation](https://docs.influxdata.com/influxdb/v1.7/tools/shell/)
it requires to be run on localhost otherwise [it does not work](https://stackoverflow.com/questions/49848830/encountering-invalid-metadata-blob-when-i-want-to-backup-data).
Incremental backups will not be used do to allow the user to delete old backups.

To restore backup you can use:
```bash
influxd restore -portable <BACKUP_DIRECTORY>
```
By default database *_internal* will be skipped.

To cope with large databases an additional option (**-I** instead of **-i**) has been added. This options creates an LVM snapshot and copies the entire influxdb directory.
This procedure has been suggested from influx support despite the fact that official documentation does not mention it.

For small databases influx dump should be more efficient and easier to restore.

## Elasticsearch

Only elasticsearch configuration should be saved, elasticsearch data should be saved using  Elasticsearch Snapshot.
Folder to be saved:

* /neteye/local/elasticsearch/conf
* /neteye/local/elasticsearch/license

**Elasticsearch data will not be saved**, please configure [Snapshot Lifecycle Management](https://www.elastic.co/guide/en/elasticsearch/reference/7.4/getting-started-snapshot-lifecycle-management.html).

## Cluster settings

Following files and directories will be saved:

* /etc/neteye-cluster 
* /etc/hosts 
* /etc/sysconfig/network-scripts/ifcfg-* to replicate network configuration
* /etc/drbd.d

## RPMS
Installed RPM list on each node.

## PCS Status
Dump current pcs status of the cluster.


