#! /bin/bash

. /neteye/shared/backup/scripts/functions.sh

TARGET_NODE=$1
BACKUP_DIR=$2
SERVICE_BACKUP_DIR="${BACKUP_DIR}/"

NET_SERVER_DIR="$SERVICE_BACKUP_DIR/server"
NET_REPORT_DIR="$SERVICE_BACKUP_DIR/report"
SHR_SERVER_DIR="$SERVICE_BACKUP_DIR/share"
SHR_OCS_DIR="$SERVICE_BACKUP_DIR/share"

mkdir -p ${NET_SERVER_DIR}
mkdir -p ${NET_REPORT_DIR}
mkdir -p ${SHR_OCS_DIR}

rsync_backup ${TARGET_NODE} "/neteye/shared/ocsinventory-server/conf" "${NET_SERVER_DIR}"
rsync_backup ${TARGET_NODE} "/neteye/shared/ocsinventory-server/data" "${NET_SERVER_DIR}"
rsync_backup ${TARGET_NODE} "/neteye/shared/ocsinventory-ocsreports/conf" "${NET_REPORT_DIR}"
rsync_backup ${TARGET_NODE} "/neteye/shared/ocsinventory-ocsreports/data" "${NET_REPORT_DIR}"


rsync_backup ${TARGET_NODE} "/usr/share/ocsinventory-server" "${SHR_OCS_DIR}"
rsync_backup ${TARGET_NODE} "/usr/share/ocsinventory-ocsreports" "${SHR_OCS_DIR}"