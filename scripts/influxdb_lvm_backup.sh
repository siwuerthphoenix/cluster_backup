#! /bin/bash
. /neteye/shared/backup/scripts/functions.sh


TARGET_NODE=$1
BACKUP_DIR=$2
SERVICE_BACKUP_DIR="${BACKUP_DIR}"

SNAP_NAME="lvinfluxdb_drbd_snap"
LV_ORIG="/dev/vg00/lvinfluxdb_drbd"
LV_MNTP="/mnt/$SNAP_NAME"
LV_SNAP="/dev/vg00/$SNAP_NAME"

# Create snapshot
ssh root@"$TARGET_NODE" /usr/sbin/lvcreate -L 5GB --snapshot --name "$SNAP_NAME" "$LV_ORIG"
ssh root@"$TARGET_NODE" mkdir -p "$LV_MNTP"
ssh root@"$TARGET_NODE" /usr/bin/mount -o nouuid "$LV_SNAP" "$LV_MNTP"/

# Backup files
mkdir -p ${SERVICE_BACKUP_DIR}
rsync_backup ${TARGET_NODE} "/mnt/$SNAP_NAME/" ${SERVICE_BACKUP_DIR}

# Cleanup
ssh root@"$TARGET_NODE" /usr/bin/umount "$LV_SNAP"
ssh root@"$TARGET_NODE" /usr/sbin/lvremove -f "$LV_SNAP"
ssh root@"$TARGET_NODE" rm -rf "$LV_MNTP"
