#! /bin/bash

. /neteye/shared/backup/scripts/functions.sh

TARGET_NODE=$1
BACKUP_DIR=$2
SERVICE_BACKUP_DIR="${BACKUP_DIR}/"

SHR_DIR="${SERVICE_BACKUP_DIR}/share"
GLPI_DIR="${SERVICE_BACKUP_DIR}/glpi"

mkdir -p $SHR_DIR
mkdir -p $GLPI_DIR

rsync_backup ${TARGET_NODE} "/neteye/shared/glpi/conf" "${GLPI_DIR}"
rsync_backup ${TARGET_NODE} "/neteye/shared/glpi/data" "${GLPI_DIR}"

rsync_backup ${TARGET_NODE} "/usr/share/glpi" "${SHR_DIR}"
