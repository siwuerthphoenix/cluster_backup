#! /bin/bash

. /neteye/shared/backup/scripts/functions.sh

TARGET_NODE=$1
BACKUP_DIR=$2
SERVER_BACKUP_DIR="${BACKUP_DIR}/nats-server/"

mkdir -p ${SERVER_BACKUP_DIR}

rsync_backup ${TARGET_NODE} "/neteye/shared/nats-server/conf" ${SERVER_BACKUP_DIR}
rsync_backup ${TARGET_NODE} "/neteye/shared/nats-server/data" ${SERVER_BACKUP_DIR}
