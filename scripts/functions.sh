#! /bin/bash

function handle_error(){
    RES=$1
    SRC_PATH="$2"

    if [[ "$RES" != "0" ]]; then
        echo "[!] Error during ${SRC_PATH} backup"
        echo "$RES ${SRC_PATH}" >> "$TMP_BACKUP_ERROR_FILE"
    fi
}

function rsync_backup() {
    TARGET_NODE=$1
    SRC_PATH=$2
    RSYNC_SERVICE_BACKUP_DIR=$3
    rsync -az root@${TARGET_NODE}:${SRC_PATH} ${RSYNC_SERVICE_BACKUP_DIR}
    RES=$?
    handle_error "$RES" "${SRC_PATH}"
}

function rsync_backup_preserve_path() {
    TARGET_NODE=$1
    SRC_PATH=$2
    RSYNC_SERVICE_BACKUP_DIR=$3
    rsync -azR root@${TARGET_NODE}:${SRC_PATH} ${RSYNC_SERVICE_BACKUP_DIR}
    handle_error $? "${SRC_PATH}"
}