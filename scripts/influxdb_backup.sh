#! /bin/bash

. /neteye/shared/backup/scripts/functions.sh

# Have to use localhost https://stackoverflow.com/questions/49848830/encountering-invalid-metadata-blob-when-i-want-to-backup-data
HOST="localhost"
PORT="8088"
TARGET_NODE=$1
TMP_BACKUP_DIR="$2/influx_tmp/"
BACKUP_DIR=$3

if [[ -z $TMP_BACKUP_DIR ]]; then
    echo "Temporary Backup dir for InfluxDB is mandatory!"
    exit 1
fi

if [[ -z $BACKUP_DIR ]]; then
    echo "Backup dir for InfluxDB is mandatory!"
    exit 1
fi

# Create temporary backup dir
ssh root@${TARGET_NODE} "mkdir -p ${TMP_BACKUP_DIR}" 

# Dump influxdb
ssh root@${TARGET_NODE} "influxd backup -portable -host ${HOST}:${PORT} ${TMP_BACKUP_DIR}"
handle_error $? "Influxdb dump"

# Copy dump to backup directory
rsync_backup ${TARGET_NODE} "${TMP_BACKUP_DIR}" "${BACKUP_DIR}"
handle_error $? "Influxdb copy"

# Cleanup temporary file
ssh root@${TARGET_NODE} "rm -rf ${TMP_BACKUP_DIR}"
