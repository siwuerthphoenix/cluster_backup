#! /bin/bash
. /neteye/shared/backup/scripts/functions.sh


TARGET_NODE=$1
BACKUP_DIR=$2
SERVICE_BACKUP_DIR="${BACKUP_DIR}"

SNAP_NAME="lvneteye_snap"
LV_ORIG="/dev/vg00/lvneteye"
LV_MNTP="/mnt/$SNAP_NAME"
LV_SNAP="/dev/vg00/$SNAP_NAME"

# Create snapshot
/usr/sbin/lvcreate -L 5GB --snapshot --name "$SNAP_NAME" "$LV_ORIG"
mkdir -p "$LV_MNTP"
/usr/bin/mount -o nouuid "$LV_SNAP" "$LV_MNTP"/

# Backup files
mkdir -p ${SERVICE_BACKUP_DIR}
rsync_backup ${TARGET_NODE} "/mnt/$SNAP_NAME/shared/influxdb/" ${SERVICE_BACKUP_DIR}

# Cleanup
/usr/bin/umount "$LV_SNAP"
/usr/sbin/lvremove -f "$LV_SNAP"
rm -rf "$LV_MNTP"