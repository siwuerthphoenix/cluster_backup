#! /bin/bash

. /neteye/shared/backup/scripts/functions.sh

TARGET_NODE=$1
BACKUP_DIR=$2
SERVICE_BACKUP_DIR="${BACKUP_DIR}/${TARGET_NODE}"

mkdir -p ${SERVICE_BACKUP_DIR}

#rsync_backup ${TARGET_NODE} "/neteye/local/elasticsearch/conf" ${SERVICE_BACKUP_DIR}
rsync_backup ${TARGET_NODE} "/neteye/local/elasticsearch/license" ${SERVICE_BACKUP_DIR}
#rsync_backup ${TARGET_NODE} "/neteye/local/elasticsearch/log" ${SERVICE_BACKUP_DIR}