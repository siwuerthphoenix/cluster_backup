#! /bin/bash

. /neteye/shared/backup/scripts/functions.sh

#TARGET_NODE=$1
BACKUP_DIR=$1
PCS_BACKUP_DIR="${BACKUP_DIR}"

mkdir -p ${PCS_BACKUP_DIR}

#ssh ${TARGET_NODE} 
pcs status > ${PCS_BACKUP_DIR}/cluster_services_status.txt

handle_error $? "Error saving pcs status"