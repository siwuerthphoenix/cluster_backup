#! /bin/bash

. /neteye/shared/backup/scripts/functions.sh

TARGET_NODE=$1
BACKUP_DIR=$2
RPM_BACKUP_DIR="${BACKUP_DIR}/${TARGET_NODE}"

mkdir -p ${RPM_BACKUP_DIR}

ssh ${TARGET_NODE} "/usr/bin/rpm -qa" > ${RPM_BACKUP_DIR}/rpm-fulllist.txt
handle_error $? "Error saving rpm list"