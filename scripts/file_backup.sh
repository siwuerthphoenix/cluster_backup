#! /bin/bash

. /neteye/shared/backup/scripts/functions.sh

TARGET_NODE=$1
BACKUP_DIR=$2
FILE_LIST=$3
FILE_BACKUP_DIR="${BACKUP_DIR}/${TARGET_NODE}"

if [[ ! -f $FILE_LIST ]]; then
    echo "File $FILE_LIST is not regular file!"
    exit 1
fi

mkdir -p ${FILE_BACKUP_DIR}

while read FILE || [ -n "$FILE" ]; 
do 
    if [[ ! -z $FILE ]]; then
        rsync_backup_preserve_path ${TARGET_NODE} "${FILE}" ${FILE_BACKUP_DIR}
    else
        echo "empty line"
    fi
done < $FILE_LIST