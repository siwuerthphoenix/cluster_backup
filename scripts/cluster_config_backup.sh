#! /bin/bash

. /neteye/shared/backup/scripts/functions.sh

TARGET_NODE=$1
BACKUP_DIR=$2
SERVICE_BACKUP_DIR="${BACKUP_DIR}/${TARGET_NODE}"

mkdir -p ${SERVICE_BACKUP_DIR}

rsync_backup ${TARGET_NODE} "/etc/hosts" ${SERVICE_BACKUP_DIR}
rsync_backup ${TARGET_NODE} "/etc/neteye-cluster" ${SERVICE_BACKUP_DIR}
rsync_backup ${TARGET_NODE} "/etc/drbd.d" "${SERVICE_BACKUP_DIR}"
rsync_backup ${TARGET_NODE} "/etc/sysconfig/network-scripts/ifcfg-*" "${SERVICE_BACKUP_DIR}/network-scripts"
