#! /bin/bash

. /neteye/shared/backup/scripts/functions.sh

TARGET_NODE=$1
BACKUP_DIR=$2
SERVICE_BACKUP_DIR="${BACKUP_DIR}"

mkdir -p ${SERVICE_BACKUP_DIR}

rsync_backup ${TARGET_NODE} "/neteye/shared/tornado_icinga2_collector/conf" ${SERVICE_BACKUP_DIR}
rsync_backup ${TARGET_NODE} "/neteye/shared/tornado_icinga2_collector/data" ${SERVICE_BACKUP_DIR}