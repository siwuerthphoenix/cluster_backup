#! /bin/bash

. /neteye/shared/backup/scripts/functions.sh

# Backup parameters

PORT=3308
SRC_LV="/dev/vg00/lvneteye"
DST_LV="/dev/vg00/lvmariadb_snap"
MOUNT_POINT="/mnt/lvmariadb_snap"
TARGET_NODE=$1
TMP_BACKUP_DIR=$2
BACKUP_DIR=$3

MYSQL_INSTANCE_RUNTIMEDIR="/tmp/mysql_backup"
PID_FILE="${MYSQL_INSTANCE_RUNTIMEDIR}/mysql_snap.pid"
SOCKET_FILE="${MYSQL_INSTANCE_RUNTIMEDIR}/mysql_snap.sock"
LOG_FILE="${MYSQL_INSTANCE_RUNTIMEDIR}/mysql_snap.log"

SLEEP_TIME=5

if [[ -z $BACKUP_DIR ]]; then
    echo "Backup dir for MySQL is mandatory!"
    exit 1
fi


# Create Backup Dir
mkdir -p $BACKUP_DIR

#Create snapshot
ssh root@${TARGET_NODE} "/usr/sbin/lvcreate -L 5GB --snapshot --name lvmariadb_snap $SRC_LV"
ssh root@${TARGET_NODE} "mkdir -p $MOUNT_POINT"
ssh root@${TARGET_NODE} "chown -R mysql:mysql $MOUNT_POINT"
ssh root@${TARGET_NODE} "mkdir -p ${MYSQL_INSTANCE_RUNTIMEDIR}; chown -R mysql:mysql ${MYSQL_INSTANCE_RUNTIMEDIR}"
ssh root@${TARGET_NODE} "mount -o nouuid $DST_LV $MOUNT_POINT"


# Create temporary backup dir
ssh root@${TARGET_NODE} "mkdir -p ${TMP_BACKUP_DIR}"
ssh root@${TARGET_NODE} "/usr/bin/nohup /usr/bin/sudo -u mysql /usr/libexec/mysqld --no-defaults --datadir=$MOUNT_POINT/shared/mysql/data --socket=${SOCKET_FILE} --port=$PORT --pid-file=${PID_FILE}  > ${LOG_FILE} 2>&1 & "

echo "[i] Sleep ${SLEEP_TIME} seconds"
sleep ${SLEEP_TIME}


ssh root@${TARGET_NODE} "mysqldump --all-databases -h 127.0.0.1 --port=$PORT > $TMP_BACKUP_DIR/all_dbs_dump.sql"

# Copy dump to backup directory
rsync_backup ${TARGET_NODE} "${TMP_BACKUP_DIR}" "${BACKUP_DIR}"
handle_error $? "Mariadb copy"
rsync_backup ${TARGET_NODE} "/root/.my.cnf" "${BACKUP_DIR}"
handle_error $? ".my.cnf copy"

# Kill mysql
PID=$(ssh root@${TARGET_NODE} "cat ${PID_FILE}")
ssh root@${TARGET_NODE} "kill -9 $PID"
echo "[i] Sleep ${SLEEP_TIME} seconds"
sleep ${SLEEP_TIME}

# Cleanup temporary file
ssh root@${TARGET_NODE} "rm -f ${TMP_BACKUP_DIR}/*"
ssh root@${TARGET_NODE} "umount $MOUNT_POINT"
ssh root@${TARGET_NODE} "lvremove -y $DST_LV"
ssh root@${TARGET_NODE} "rm -rf $MOUNT_POINT"
