#! /bin/bash

. /neteye/shared/backup/scripts/functions.sh

TARGET_NODE=$1
BACKUP_DIR=$2
SERVICE_BACKUP_DIR="${BACKUP_DIR}/${TARGET_NODE}"

cd /neteye/local/
list_dir=$( find . -maxdepth 1 -type d | egrep -v '/update|/conf')
for DIR in list_dir
do
    if ssh root@${TARGET_NODE} '[ -d "/neteye/local/${DIR:-2}/conf" ]'
    then
        mkdir -p "$SERVICE_BACKUP_DIR/${DIR:-2}"
        BACKUP_DIR="$SERVICE_BACKUP_DIR/${DIR:-2}"
        rsync_backup ${TARGET_NODE} "/neteye/local/${DIR:-2}/conf" ${BACKUP_DIR}
    fi
done

# Local scripts
if ssh root@${TARGET_NODE} '[ -d "/neteye/local/scripts" ]'
then
    mkdir -p "$SERVICE_BACKUP_DIR"
    rsync_backup ${TARGET_NODE} "/neteye/local/scripts" "${SERVICE_BACKUP_DIR}"
fi