#! /bin/bash

. /neteye/shared/backup/scripts/functions.sh

TARGET_NODE=$1
BACKUP_DIR=$2
SERVICE_BACKUP_DIR="${BACKUP_DIR}/"

NET_HTTPD_DIR="$SERVICE_BACKUP_DIR/shared"
ETC_HTTPD_DIR="$SERVICE_BACKUP_DIR/etc"

mkdir -p ${NET_HTTPD_DIR}
mkdir -p ${ETC_HTTPD_DIR}

rsync_backup ${TARGET_NODE} "/neteye/shared/httpd/conf" "${NET_HTTPD_DIR}"

rsync_backup ${TARGET_NODE} "/etc/httpd/conf" "${ETC_HTTPD_DIR}"
rsync_backup ${TARGET_NODE} "/etc/httpd/conf.d" "${ETC_HTTPD_DIR}"