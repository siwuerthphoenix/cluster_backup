#! /bin/bash

echo "[+] Create backup installation dir..."
mkdir -p /neteye/shared/backup/

echo "[+] Copy scripts..."
cp -a scripts /neteye/shared/backup/
cp backup.sh /neteye/shared/backup/
cp backup_rotate.sh /neteye/shared/backup/
cp check_neteye_backup.sh /neteye/shared/monitoring/plugins/

echo "[+] Set execution permissions..."
chmod +x -R /neteye/shared/backup/*.sh
chmod +x -R /neteye/shared/backup/scripts/*.sh

echo "[+] Install dependences..."
yum install -y RPMS/*.rpm

echo "[+] Installation completed, now execute a backup manually to complete key exchange and check correct execution"
