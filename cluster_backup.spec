%define install_dir        /neteye/shared/backup
%define _debugsource_template %{nil}

Name:           cluster_backup
Version:        0.2.0
Release:        1
Summary:        cluster_backup package

Group:          Applications/System
License:        GPL v3
URL:            https://bitbucket.org/alessandro_valentini/cluster_backup/
Source0:        %{name}.tar.gz

Requires: lbzip2

%description
This package provides a set of scripts to backup a 
NetEye4 installation, both single node and cluster.

%prep
%setup -c

%build

%install
mkdir -p %{buildroot}%{install_dir}/
mkdir -p %{buildroot}%{install_dir}/scripts
mkdir -p %{buildroot}%{install_dir}/doc

%{__install} backup.sh  %{buildroot}%{install_dir}/
%{__install} *.conf.tpl %{buildroot}%{install_dir}/
%{__install} scripts/*.sh  %{buildroot}%{install_dir}/scripts
%{__install} README.md  %{buildroot}%{install_dir}/doc/

%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%{install_dir}/

%changelog

* Fri Apr 08 2022 Alessandro Valentini <alessandro.valentini@wuerth-phoenix.com> - 0.2.0-1
- Switch from mylvmbackup to mysql dump with lvm snapshot
- Make spec file compatible with rhel8 and standard neteye projects

* Fri Apr 02 2020 Alessandro Valentini <alessandro.valentini@wuerth-phoenix.com> - 0.1.0-1
- Create RPM for cluster backup
