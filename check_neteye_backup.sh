#! /bin/bash

# This check looks for backup more recent than warning and critical given value

while getopts ":c:d:hw:" opt; do
  case ${opt} in
    h )
        echo "This check looks for backup more recent than warning and critical given time."
        echo "Time is expressed in minutes for example 1440 means 24 hours."
        echo "Usage: "
        echo "check_neteye_backup -d <backup_dir> -w <warning_minutes> -c <critical_minutes>"
        exit 0
      ;;
    c ) CRITICAL="$OPTARG"
      ;;
    d ) BACKUP_DIR="$OPTARG"
      ;;
    w ) WARN="$OPTARG"
      ;;
    \? ) echo "Option: -$OPTARG is not supported! Use -h option for help"
       exit 1
    ;;
  esac
done

if [[ -z ${CRITICAL} ]];
then
    echo "-c param is mandatory: $CRITICAL"
    exit 2
fi

if [[ -z ${BACKUP_DIR} ]];
then
    echo "-d param is mandatory"
    exit 2
fi

if [[ -z ${WARN} ]];
then
    echo "-w param is mandatory"
    exit 2
fi


if [[ -z $(find "$BACKUP_DIR" -maxdepth 1 -name "*.tar.bz2" -mmin -$CRITICAL) ]];
then
        echo "CRITICAL: No backup more recent than $CRITICAL minutes found";
        exit 2
fi

if [[ -z $(find "$BACKUP_DIR" -maxdepth 1 -name "*.tar.bz2" -mmin -$WARN) ]];
then                                                                                                                                                                                                                                                 echo "WARNING: No backup more recent than $WARN minutes found";
        exit 1
fi